<?php

namespace App\Http\Controllers;

use App\Events\Visit;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Modules\Article\Entities\Article;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Setting;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Page\Entities\Page;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function website()
    {

        try {

            $setting=Setting::latest()->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($setting->seo->title);
            SEOMeta::setDescription($setting->seo->description);
            SEOMeta::setCanonical(env('APP_URL'));
            return view('template.index');
        }catch (\Exception $exception){

        }
    }
    public function generateSiteMap(){
        Sitemap::create()

            ->add(Url::create('/')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                ->setPriority(0.2))

            ->add(Url::create('/about-us')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2))

            ->add(Url::create('/about-us')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2))

            ->add(Url::create('/products')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2))

            ->add(Url::create('/contact-us')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2))

            ->add(Url::create('/events')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2))

            ->add(Url::create('/downloads')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2))

            ->writeToFile(public_path('sitemap.xml'));
    }
    public function articles(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('articles');
            $items=Article::latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles');
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){

        }
    }

    public function singleArticle($obj){

        try {
            $item=Article::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.articles.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function informations(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('اخبار');
            $items=Information::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/informations');
            return view('template.informations.index',compact('items'));

        }catch (\Exception $exception){

        }
    }

    public function singleInformation($obj){

        try {
            $item=Information::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.informations.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function portfolios(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('portfolios');
            $items=Portfolio::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/portfolios');
            return view('template.portfolios.index',compact('items'));

        }catch (\Exception $exception){

        }
    }

    public function singlePortfolio($obj){

        try {
            $item=Portfolio::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.portfolios.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function products(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('products');
            $items=Product::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.products.index',compact('items'));

        }catch (\Exception $exception){

        }
    }
    public function downloads(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('downloads');
            SEOMeta::setCanonical(env('APP_URL').'/downloads');
            return view('template.downloads.index');

        }catch (\Exception $exception){

        }
    }
    public function categoriesProduct(Request $request, $category){
        try {
            $item=Category::whereSlug($category)->first();

            $product_categories=Product::whereCategory($item->id)->get();

            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('categories');
            SEOMeta::setCanonical(env('APP_URL').'/categories');
            return view('template.categories.products.index',compact('product_categories'));

        }catch (\Exception $exception){
            return abort('404');
        }
    }
    public function events(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('events');
            SEOMeta::setCanonical(env('APP_URL').'/events');
            return view('template.events.index');

        }catch (\Exception $exception){

        }
    }

    public function singleProduct($obj){

        try {
             $item=Product::whereSlug($obj)->firstOrFail();
            $medias= $item->getMedia(config('cms.collection-images'));
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
             event(new Visit($item));
            return view('template.products.single',compact('item','medias'));
        }catch (\Exception $exception){

        }
    }
    public function singleMember($obj){

        try {
            $item=Member::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            return view('template.members.single',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function register(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('register');
            SEOMeta::setDescription('register');
            SEOMeta::setKeywords('register');
            SEOMeta::setCanonical(env('APP_URL').'/user/register');
            return view('template.users.register',compact('item'));
        }catch (\Exception $exception){

            return abort('404');
        }
    }
    public function login(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('login');
            SEOMeta::setDescription('login');
            SEOMeta::setKeywords('login');
            SEOMeta::setCanonical(env('APP_URL').'/user/login');
            return view('template.users.login',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public function panel(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('admin panel');
            SEOMeta::setDescription('admin panel');
            SEOMeta::setKeywords('admin panel');
            SEOMeta::setCanonical(env('APP_URL').'/dashboard');
            return view('template.users.panel',compact('item'));
        }catch (\Exception $exception){

        }
    }
    public  function contactUs(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('contact us');
            SEOMeta::setDescription('contact us');
            SEOMeta::setKeywords('contact us');
            SEOMeta::setCanonical(env('APP_URL').'/contact-us');
            return view('template.pages.contact_us',compact('item'));
        }catch (\Exception $exception){
            return abort('404');
        }
    }
    public  function aboutUs(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('about us');
            SEOMeta::setDescription('about us');
            SEOMeta::setKeywords('about us');
            SEOMeta::setCanonical(env('APP_URL').'/about-us');
            return view('template.pages.about_us',compact('item'));
        }catch (\Exception $exception){
            return abort('404');
        }
    }

    public  function page($page){
        try {
                $page = Page::whereHref($page)->first();
                SEOMeta::setTitleDefault(\setting('name'));
                SEOMeta::setTitle($page->seo->title);
                SEOMeta::setDescription($page->seo->description);
                SEOMeta::setKeywords($page->seo->keyword);
                SEOMeta::setCanonical($page->seo->canonical);
                return view('template.pages.empty_page', compact('page'));
        }catch (\Exception $exception){
            return abort('404');
        }
    }
}
