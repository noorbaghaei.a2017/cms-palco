<?php

namespace App\View;

use Modules\Core\Entities\Contact;
use Modules\Core\Entities\User;
use Illuminate\View\View;
use Modules\Article\Entities\Article;
use Modules\Information\Entities\Information;
use Modules\Product\Entities\Product;

class adminComposer{

    public function compose(View $view){

        $view->with('articlescount', Article::latest()->count());
        $view->with('productscount', Product::latest()->count());
        $view->with('informationscount', Information::latest()->count());
        $view->with('contact_count', Contact::latest()->count());
        $view->with('userscount', User::latest()->count());
        $view->with('lastArticles', Article::latest()->take(5)->get());
        $view->with('lastUsers', User::latest()->take(5)->get());
        $view->with('lastProducts', Product::latest()->take(5)->get());
        $view->with('lastInformations', Information::latest()->take(5)->get());

    }

}
