@extends('core::layout.panel')
@section('pageTitle', 'مشاهده پروفایل')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images') && statusUrl(findAvatar($item->email,200))===200)
                                    <img style="height: auto" src="{{findAvatar($item->email,200)}}" alt="" >

                                @elseif(statusUrl(findAvatar($item->email,200))!==200)

                                    <img style="width: 80px;height: auto" src="{{asset('img/no-img.gif')}}">
                                @else

                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> نام  : </h6>
                                <h4 style="padding-top: 35px">    {{fullName($item->first_name,$item->last_name)}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> آدرس ایمیل : </h6>
                                <p>    {{$item->email}}</p>
                            </div>

                            <div>
                                <h6 style="padding-top: 35px"> موبایل  : </h6>
                                <p>    {{$item->mobile}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <small>
                            با استفاده از فرم زیر میتوانید پروفایل خود را مشاهده کنید.
                        </small>
                        </div>
                        <div class="box-divider m-a-0"></div>
                        <div class="box-body">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                                @include('core::layout.alert-danger')
                            @if(\Illuminate\Support\Facades\Session::has('error'))
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{\Illuminate\Support\Facades\Session::get('error')}}</li>
                                    </ul>
                                </div>
                            @endif

                                <form action="{{route('profile.update', ['user' => $item->token])}}" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{method_field('PATCH')}}
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="firstname" class="form-control-label">نام</label>
                                        <input type="text" name="firstname" class="form-control" id="firstname" value="{{$item->first_name}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="lastname" class="form-control-label">نام خانوادگی</label>
                                        <input type="text" name="lastname" class="form-control" id="lastname" value="{{$item->last_name}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="mobile" class="form-control-label">شماره موبایل</label>
                                        <input  type="text" name="mobile" class="form-control" id="mobile" value="{{$item->mobile}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="title" class="form-control-label">تصویر شاخص </label>
                                        @include('core::layout.load-single-image')
                                    </div>

                                </div>
                                <div class="row form-group">

                                    <div class="col-sm-4">
                                        <label for="current_password" class="form-control-label">رمزعبور فعلی</label>
                                        <input type="password" name="current_password" class="form-control" id="current_password">
                                        <small class="help-block text-warning">اگر میخواهید تغییر ندهید، خالی رها کنید</small>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="new_password" class="form-control-label">رمز عبور جدید</label>
                                        <input type="password" name="new_password" class="form-control" id="new_password">
                                        <small class="help-block text-warning">اگر میخواهید تغییر ندهید، خالی رها کنید</small>
                                    </div>
                                </div>

                                <div class="form-group row m-t-md">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-sm text-sm">بروزرسانی </button>
                                    </div>
                                </div>
                            </form>
                            <hr/>
                            <br/>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
