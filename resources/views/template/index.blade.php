@extends('template.app')
@section('content')

    <nav class="fix-nav">
        <a href="#section1" class="active"></a>
        <a href="#section2"></a>
        <a href="#section3"></a>
        <a href="#section4"></a>
        <a href="#section5"></a>
    </nav>

@include('template.sections.sliders')


@include('template.sections.products')

    <div  id="section3">
       @include('template.sections.events')

        <section>
            <div class="sec-container">
                <div class="container">
        <span class="sec-container-txt">
           {{$setting->excerpt}}

               </span>
                </div>
            </div>
        </section>

       @include('template.sections.customer')
    </div>

    @include('template.sections.store')

@endsection


@section('head')


    <link rel="stylesheet" href="{{asset('template/plugins/swiper/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/owl-carousel/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/anime/animate.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/main.css')}}">

@endsection

@section('script')

    <script src="{{asset('template/plugins/swiper/swiper.min.js')}}"></script>
    <script src="{{asset('template/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('template/plugins/slideNav/slideNav.min.js')}}"></script>
    <script src="{{asset('template/js/main.js')}}"></script>


@endsection

@section('seo')


    <meta name="author" content="amin nourbaghaei">

    <meta name="robots" content="noindex,nofollow" />

@endsection


