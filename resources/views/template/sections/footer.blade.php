<div id="section5">
    <footer >
        <div class="col-lg-6 col-md-6 col-sm-6 col-9 contact-button">
            <a href="{{route('contact-us')}}"> Contact Us Now! </a>
        </div>
        <div class="social-container">
            <ul class="social-list">
                @foreach($brands as $brand)
                <li class="social-item">
                    <a href="{{$brand->href}}" target="{{$brand->target}}">
                        <div class="brands-img">
                            <img src="{{$brand->getFirstMediaUrl('images')}}" alt="">
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="end-sec">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="logo-img">
                            @if(!$setting->Hasmedia('logo'))
                                <img style="width: 120px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                            @else
                                <img style="width: 120px;height: auto" src="{{$setting->getFirstMediaUrl('logo')}}" alt="" class="img-responsive">


                            @endif
                        </div>
                    </div>
                    <div class="col-lg-8  col-md-8 col-sm-8 d-flex align-items-center">
                        <div class="col-lg-12">
                            <div class="col-lg-12 info-contain">
                                <div class="row">
                                    <div class="col-lg-6  phone-content">
                                        <div class="phone"><i class="fal fa-phone-alt"></i></div>
                                        <div class="phone-num">
                                            <span>Tel : </span>
                                            <span> +49 {{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}} </span>
                                        </div>
                                        <div class="fax-num">
                                            <span>Fax : </span>
                                            <span> +49 {{isset(json_decode($setting->fax,true)[0])  ? json_decode($setting->fax,true)[0] :"" }} </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6  phone-content">
                                        <div class="phone"><i class="fal fa-envelope"></i></div>
                                        <div class="mail">
                                            <span>{{$setting->email}} </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 footer-menu">
                                <ul class="footer-menu-list">
                                    @foreach($bottom_menus as $menu)
                                    <li class="footer-menu-item"><a href="{{$menu->href}}"> {{$menu->title}} </a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="{{asset('template/plugins/jquery/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('template/plugins/bootstrap/popper.js')}}"></script>
<script src="{{asset('template/plugins/bootstrap/bootstrap.min.js')}}"></script>

@yield('script')

</body>
</html>




