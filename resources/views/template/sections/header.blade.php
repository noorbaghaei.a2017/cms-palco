<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="{{asset('template/images/logo/logo.png')}}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {!! SEO::generate() !!}
    @yield('seo')

{{--    Google Search Console--}}

    <meta name="google-site-verification" content="y0hgzEgz7pV1ZZsA7r88a0uwfqfz7oG8i_E5Jmcm8MY" />

    <link rel="stylesheet" href="{{asset('template/plugins/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/fonts/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/header.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/responsive.css')}}">
    @yield('head')
</head>
<body>

@include('template.sections.menu')






