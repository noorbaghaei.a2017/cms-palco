@if(\Modules\Core\Helper\CoreHelper::hasCustomer())
<section>
    <div class="owl-pic-container">
        <div class="picture-carousel owl-carousel owl-theme">
            @foreach($customers as $customer)
                <div class="item">
                    <a href="#">
                        <div class="image-content">
                            <img style="width: 120px;height: auto;margin: 0 auto" src="{{$customer->getFirstMediaUrl('images')}}" alt="">
                        </div>
                    </a>
                </div>
            @endforeach

        </div>
    </div>
</section>
@endif
