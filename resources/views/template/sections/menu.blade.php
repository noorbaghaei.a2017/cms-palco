<header>
    <nav class="navbar navbar-expand-lg navbar-dark navigation">
        <a class="navbar-brand" href="{{route('front.website')}}">
            @if(!$setting->Hasmedia('logo'))
                <img style="width: 120px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


            @else
                <img style="width: 120px;height: auto" src="{{$setting->getFirstMediaUrl('logo')}}" alt="" class="img-responsive">


            @endif
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fal fa-stream"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
               @foreach($top_menus as $menu)

                       @if(childCount($menu->id) == 0)

                            <li class="nav-item">
                                <a class="nav-link" href="{{$menu->href}}"> {{$menu->symbol}} </a>
                            </li>

                        @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="{{$menu->href}}" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$menu->symbol}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">


                                <div class="container">
                                    <div class="row">
                                       @if($menu->columns==4)
                                            @include('core::layout.menu.four-column')

                                        @elseif($menu->columns==3)

                                    @include('core::layout.menu.three-column')

                                          @elseif($menu->columns==2)
                                            @include('core::layout.menu.tow-column')

                                        @elseif($menu->columns==1)

                                            @include('core::layout.menu.one-column')
                                        @endif
                                    </div>
                                </div>
                                <!--  /.container  -->


                            </div>
                        </li>
                        @endif



                @endforeach

            </ul>
        </div>

    </nav>

</header>
