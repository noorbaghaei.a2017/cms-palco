@if(\Modules\Core\Helper\CoreHelper::hasSlider())
<div  id="section1">
    <section >
        <!-- Swiper -->
        <div class="swiper-container gallery-top ">
            <div class="swiper-wrapper">
                @foreach($sliders as $slider)
                <div class="swiper-slide"><a href="#"><img src="{{$slider->getFirstMediaUrl('images')}}" alt=""></a>
                </div>

                @endforeach
            </div>

            <div class="swiper-container gallery-thumbs">
                <div class="swiper-wrapper">
                    @foreach($sliders as $slider)
                    <div class="swiper-slide"><a>
                            <img src="{{$slider->getFirstMediaUrl('images','thumb')}}" alt=""></a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</div>
@endif
