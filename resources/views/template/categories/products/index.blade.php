@extends('template.app')
@section('content')

    <section  class="bread-container">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10">
                <ul>
                    <li><a href="{{route('products')}}"> Products </a></li>

                </ul>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid box_style">
            <div class="product-carousel owl-carousel owl-theme">

                @foreach($product_categories as $product)
                    <div class="item">
                        <div class=" d-flex justify-content-center">
                            <div class="col-xl-10 col-lg-12 ">
                                <a href="{{route('products.single',['product'=>$product->slug])}}">
                                    <div class="col-lg-12 product-img-content highlight">
                                        <span> {{$product->title}} </span>
                                        <div class="img-contain">
                                            <img src="{{$product->getFirstMediaUrl('images','list')}}" alt="">
                                        </div>
                                    </div>
                                </a>
                                <div class="col-lg-12 product-des box_style">
                                    <div class="des-container">
                                        {!! $product->text !!}

                                        <div class="des-title">Options </div>
                                        <ul class="des-list">
                                            @if(isset($product->option) && !is_null($product->option) && $product->option!='null')
                                                @foreach(json_decode($product->option,true) as $value)
                                                    <li class="des-item">{{$value}} </li>

                                                @endforeach
                                            @endif


                                        </ul>
                                        <div class="des-title">Soft Ability </div>
                                        <ul class="des-list">
                                            @if(isset($product->ability) && !is_null($product->ability) && $product->ability!='null')
                                                @foreach(json_decode($product->ability,true) as $value)
                                                    <li class="des-item">{{$value}} </li>

                                                @endforeach
                                            @endif


                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>

@endsection

@section('head')

    <link rel="stylesheet" href="{{asset('template/plugins/anime/animate.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/owl-carousel/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/products.css')}}">

@endsection



@section('script')

    <script src="{{asset('template/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('template/js/product.js')}}"></script>

@endsection


@section('seo')


    <meta name="author" content="amin nourbaghaei">

    <meta name="robots" content="noindex,nofollow" />

@endsection



