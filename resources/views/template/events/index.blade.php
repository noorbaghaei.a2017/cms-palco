@extends('template.app')
@section('content')


    <section  class="bread-container">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10">
                <ul>
                    <li><a href="#"> Events & News  </a></li>
                </ul>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid plp-container"><div class="plp-box"></div></div>

        <div class="news-slider-content">
            <div class="container-fluid ">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-7 col-md-7 ">
                        <div class="swiper-container news-left-slider">
                            <!-- Swiper -->
                            <div class="swiper-wrapper">
                                @foreach($events as $key=>$event)
                                <div class="swiper-slide">
                                    <div class="box-container">
                                        <div class="left-main-box">
                                            <div class="row text-box">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="img-box">
                                                        <img src="{{$event->getFirstMediaUrl('images','thumb')}}" alt="" width="220px" height="115px">
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8">
                                                    <a href="#">
                                                        <div class="right-text">

                                                           {!! $event->text !!}
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5  right-slider-content">
                        <!-- Swiper -->
                        <div class="swiper-container news-right-slider">
                            <div class="swiper-wrapper">
                                @foreach($events as $key=>$event)
                                    @if($key % 2 ==0)
                                        <div class="swiper-slide"> <div class="box-container">
                                                <div class="top-border"></div>
                                                <div class="main-box">
                                                    <div class="text-box">
                                                        <div class="left-text">
                                                            <a href="#"><h2> {{$event->title}} </h2></a>
                                                            <span> {{$event->country}} </span>
                                                        </div>
                                                        <div class="right-text">
                                                            <span> {{$event->created_at}} </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div></div>
                                    @else
                                        <div class="swiper-slide"><div class="box-container">
                                                <div class="top-border"></div>
                                                <div class="main-box second">
                                                    <div class="text-box">
                                                        <div class="left-text">
                                                            <a href="#"><h2> {{$event->title}}  </h2></a>
                                                            <span> {{$event->country}} </span>
                                                        </div>
                                                        <div class="right-text">
                                                            <span> {{$event->created_at}} </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div></div>
                                    @endif


                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="sec-container">
            <div class="container">
        <span class="sec-container-txt">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim eum omnis perferendis voluptate. Aliquam aut deleniti deserunt dolores inventore maiores minima mollitia veritatis. Architecto blanditiis delectus exercitationem fugiat pariatur, temporibus.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim eum omnis perferendis voluptate. Aliquam aut deleniti deserunt dolores inventore maiores minima mollitia veritatis. Architecto blanditiis delectus exercitationem fugiat pariatur, temporibus.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim eum omnis perferendis voluptate. Aliquam aut deleniti deserunt dolores inventore maiores minima mollitia veritatis. Architecto blanditiis delectus exercitationem fugiat pariatur, temporibus.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim eum omnis perferendis voluptate. Aliquam aut deleniti deserunt dolores inventore maiores minima mollitia veritatis. Architecto blanditiis delectus exercitationem fugiat pariatur, temporibus.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim eum omnis perferendis voluptate. Aliquam aut deleniti deserunt dolores inventore maiores minima mollitia veritatis. Architecto blanditiis delectus exercitationem fugiat pariatur, temporibus.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim eum omnis perferendis voluptate. Aliquam aut deleniti deserunt dolores inventore maiores minima mollitia veritatis. Architecto blanditiis delectus exercitationem fugiat pariatur, temporibus.
        </span>
            </div>
        </div>
    </section>

    <section>
        <div class="container news-line"></div>
        <!-- Swiper -->
        <div class="swiper-container company-gallery-top">
            <div class="swiper-wrapper">
                @foreach($stores as $store)
                <div class="swiper-slide" style="background-image:url({{$store->getFirstMediaUrl('images')}})"></div>
                @endforeach
            </div>

        </div>
        <div class="swiper-container company-gallery-thumbs">
            <div class="swiper-wrapper">
                @foreach($stores as $store)
                <div class="swiper-slide" style="background-image:url({{$store->getFirstMediaUrl('images','thumb')}})"></div>
                @endforeach
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-black" style="height: 30px"></div>
            <div class="swiper-button-prev swiper-button-black" style="height: 30px"></div>
        </div>
    </section>

@endsection

@section('head')

    <link rel="stylesheet" href="{{asset('template/plugins/swiper/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/anime/animate.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/main.css')}}">

@endsection



@section('script')

    <script src="{{asset('template/plugins/easy/jquery.easy-ticker.js')}}"></script>
    <script src="{{asset('template/plugins/swiper/swiper.min.js')}}"></script>
    <script src="{{asset('template/js/events.js')}}"></script>

@endsection

@section('seo')


    <meta name="author" content="amin nourbaghaei">

    <meta name="robots" content="noindex,nofollow" />

@endsection



