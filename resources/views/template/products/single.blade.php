﻿@extends('template.app')
@section('content')

    <section  class="bread-container">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10">
                <ul>
                    <li><a href="{{route('products')}}"> Products </a></li>
                    <li><a href="{{route('products.single',['product'=>$item->slug])}}"> {{$item->title}} </a></li>
                </ul>
            </div>
        </div>
    </section>

    <!-- SECTION -->
    <section class="section box_style">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">


                <!-- Product main img -->
                <div class="col-lg-5 order-lg-2 col-md-push-2">
                    <div id="product-main-img">
                        <div class="product-preview">
                            <img src="{{$item->getFirstMediaUrl('images')}}" alt="">
                        </div>

                        @foreach($medias as $media)
                            <div class="product-preview">
                                <img src="{{$media->getUrl()}}" alt="">
                            </div>
                        @endforeach


                    </div>
                </div>
                <!-- /Product main img -->
                <!-- Product thumb imgs -->
                <div class="col-lg-2 order-lg-1  col-md-pull-5">
                    <div id="product-imgs">
                        <div class="product-preview">
                            <img src="{{$item->getFirstMediaUrl('images','thumb')}}" alt="">
                        </div>

                        @foreach($medias as $media)
                            <div class="product-preview">
                                <img src="{{$media->getUrl()}}" alt="">
                            </div>
                        @endforeach


                    </div>
                </div>
                <!-- /Product thumb imgs -->

                <div class="col-lg-5  order-lg-3 single-product-title">
                    <h4>{{$item->title}}</h4>
                    <p>{{$item->excerpt}}</p>
                </div>
            </div>

            <!-- /row -->
        </div>
        <!-- /container -->
        <div class="description-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 product-list-des">
                        <div class="des-container">
                            <div class="des-title">Soft Ability </div>
                            <ul class="des-list">
                                @if(isset($item->ability) && !is_null($item->ability) && $item->ability!='null')
                                @foreach(json_decode($item->ability,true) as $value)
                                <li class="des-item">{{$value}}</li>

                                @endforeach
                                    @endif

                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 product-list-des">
                        <div class="des-container">
                            <div class="des-title">Options : </div>
                            <ul class="des-list">
                                @if(isset($item->option) && !is_null($item->option) && $item->option!='null')
                                @foreach(json_decode($item->option,true) as $value)
                                <li class="des-item">{{$value}} </li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /SECTION -->
   @include('template.sections.accessories')
@endsection

@section('head')

    <link rel="stylesheet" href="{{asset('template/plugins/anime/animate.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/slick/slick-theme.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/single-product.css')}}">

@endsection



@section('script')

    <script src="{{asset('template/plugins/slick/slick.min.js')}}"></script>
    <script src="{{asset('template/plugins/slick/jquery.zoom.min.js')}}"></script>
    <script src="{{asset('template/js/single-product.js')}}"></script>

@endsection

@section('seo')

    @if(isset($item->seo->author))
    <meta name="author" content="{{$item->seo->author}}">
    @endif

    @if(isset($item->seo->robots))
        <meta name="robots" content="{{implode(",",json_decode($item->seo->robots,true))}}" />
    @endif




@endsection



