@extends('template.app')
@section('content')

    <section  class="bread-container">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10">
                <ul>
                    <li><a href="#"> Contact Us  </a></li>
                </ul>
            </div>
        </div>
    </section>

    <section>
        <div class=" img-content">
            <img class="lazy" src="{{asset('template/images/contact-us.jpg')}}" data-src="images/Capture-15414x400.JPG" alt="">
        </div>
        <div class="container information">
            <div class="row">
                <div class="col-lg-4">
                    <div class="col-lg-12 d-flex justify-content-center icon-contain box_style"><i class="fal fa-map-marker-alt"></i></div>
                    <div class="col-lg-12 info-text box_style">
                        {{$setting->address}}
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="col-lg-12 d-flex justify-content-center icon-contain box_style">
                        <div class="icon-box"><i class="fal fa-phone-alt"></i></div>
                    </div>
                    <div class="col-lg-12 info-text box_style">
                        <div class="phone-num">
                            <span>Tel : </span>
                            <span> +49 {{$setting->pre_phone}} {{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}} </span>
                        </div>
                        <div class="fax-num">
                            <span>Fax : </span>
                            <span> +49 {{$setting->pre_phone}} {{isset(json_decode($setting->fax,true)[0])  ? json_decode($setting->fax,true)[0] :"" }} </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="col-lg-12 d-flex justify-content-center icon-contain box_style">
                        <div class="icon-box"><i class="fal fa-envelope"></i></div>
                    </div>
                    <div class="col-lg-12 info-text box_style">
                        <div class="mail">
                            <span>{{$setting->email}} </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid contact-f box_style">
            <div class="row">
                <div class="col-lg-5 contact-form">
                    @include('core::layout.alert-success')
                    @include('core::layout.alert-danger')
                    <span>Contact Forms</span>
                    <form action="{{route('contacts.store')}}" method="POST" id="form-contact" class="form-contact">
                        @csrf
                        @method('POST')
                        <div class="legend">
                            <label for="name">Name : </label>
                            <input type="text" name="name" id="name">
                        </div>
                        <div class="legend">
                            <label for="last_name">Last Name : </label>
                            <input type="text" name="last_name" id="last_name">
                        </div>
                        <div class="legend">
                            <label for="email">* E-mail : </label>
                            <input type="text" name="email" id="email">
                        </div>
                        <div class="legend">
                            <label for="telephone">* Telephone : </label>
                            <input type="text" pattern="[0-9]*" name="telephone" id="telephone">
                        </div>
                        <div class="legend">
                            <label for="remark">Remark : </label>
                            <textarea name="remark" id="remark" cols="30" rows="5"></textarea>
                        </div>
                        <div class="col-lg-12 mt-4 d-flex justify-content-center">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Submit"/>
                        </div>
                    </form>
                </div>
                <div class="col-lg-7 map-content">
                    <div style="width: 100%">

                        {!! $setting->google_map !!}
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </section>

@endsection


@section('head')


    <link rel="stylesheet" href="{{asset('template/css/contact.css')}}">


@endsection

@section('script')

    <script src="{{asset('template/plugins/validate/jquery.validate.min.js')}}"></script>
    <script src="{{asset('template/plugins/validate/additional-methods.min.js')}}"></script>
    <script src="{{asset('template/plugins/lazy/lazy.min.js')}}"></script>
    <script src="{{asset('template/js/download.js')}}"></script>
    <script src="{{asset('template/js/validation.js')}}"></script>


@endsection

@section('seo')


        <meta name="author" content="amin nourbaghaei">

        <meta name="robots" content="noindex,nofollow" />

@endsection


