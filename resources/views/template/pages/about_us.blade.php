@extends('template.app')
@section('content')

    <section  class="bread-container">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10">
                <ul>
                    <li><a href="#"> About us </a></li>
                    <li><a href="#"> Company Profile </a></li>
                </ul>
            </div>
        </div>
    </section>

    <section class="box_style">
        <div class="container-fluid tab-contain">
            <div class="row">
                <div class="col-lg-3 d-flex justify-content-end">
                    <div class="col-lg-11 col-md-12 mb-3">
                        <ul class="nav nav-pills flex-column" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Company Profile </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"> OurTeam </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"> Patents & Certifications </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="strength-tab" data-toggle="tab" href="#strength" role="tab" aria-controls="strength" aria-selected="false"> Company Strength </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.col-md-4 -->
                <div class="col-lg-9 descriptoin-content box_style">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active profile-contain" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="container tab-1-content box_style" >
                                <div class="col-lg-9 col-md-9 col-sm-9 col-11 mt-4 mb-5 tab-1-txt">
                                    Palcotek GmbH
                                </div>
                                <div class="col-lg-7 mb-4 tab-1-txt-des" style="text-align: justify">
                                    Palcotek GmbH, a German based company founded in 2012, has focused its core activity on medical devices such as patient care monitor from its beginning. This company with a global trading experience and a creative and international team is ready to offer 3 types Patient care monitors SAFIRA, VECTRA and VISTA. Addint to the list of products, is the Syringe pump SG 100, result of another advance technological effort. Our goal is high quality performance.
                                </div>

                            </div>

                            <!-- Swiper -->
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    @foreach($stores as $store)
                                    <div class="swiper-slide" style="background-image:url({{$store->getFirstMediaUrl('images')}})"></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane clip-content fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                            <div class="col-lg-12 clip-footer-content box_style">
                                <div class="col-lg-6 float-right">
                                    <div class="col-lg-12 line-1"><h1>WE BELIEVE IN</h1> </div>
                                    <div class="col-lg-12 line-2"><h1> OUR TEAM </h1></div>
                                    <div class="underline"><span></span></div>
                                    <div class="col-lg-12">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, eveniet earum. Sed accusantium eligendi molestiae quo hic velit nobis et, tempora placeat ratione rem blanditiis voluptates vel ipsam? Facilis, earum!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-poss">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',1)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                            <div class="title"> {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-pos">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',2)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                        <div class="title"> {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-pos-1">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',3)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                            <div class="title"> {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-pos-2"></div>
                                </div>
                            </div>
                            <div class="row clip-row-1">
                                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-poss-3" style="background: #dadada"></div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-poss-4">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',4)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                            <div class="title"> {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3 clip-box" >
                                    <div class="clip-section clip-pos-5">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',5)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                        <div class="title">  {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-pos-6">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',5)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                        <div class="title">  {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class=" row clip-row-1">
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-pos-7">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',6)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                        <div class="title">  {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-poss-9">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',7)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                            <div class="title">  {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-pos-8">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',8)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                        <div class="title">  {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="row clip-row-1">
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-poss-7">
                                        @if(!is_null($member=\Modules\Member\Entities\Member::orderBy('order','desc')->where('order',9)->first()))
                                        <img class="lazy" src="{{asset('template/images/logo/placeholder.png')}}" data-src="{{$member->getFirstMediaUrl('images')}}" alt="">
                                            <div class="title">  {{$member->role()->first()->title}} </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 clip-box" >
                                    <div class="clip-section clip-poss-4" style="background: #d0deee"></div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
{{--content--}}
                        </div>
                        <div class="tab-pane fade" id="strength" role="tabpanel" aria-labelledby="strength-tab">
                            <div class="container triangle-content">
                                <div class="col-lg-12 tri-content triangle">
                                    <div class="row">
                                        <div class="col-lg-3 "><div class="tri-1"></div></div>
                                        <div class="col-lg-3 ">
                                            <div class="tri-2"></div>
                                        </div>
                                        <div class="col-lg-3 tri-3"></div>
                                        <div class="col-lg-3 tri-4"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 tri-content ">
                                    <div class="row">
                                        <div class="col-lg-3"> <div class="tri-7"></div></div>
                                        <div class="col-lg-3">
                                            <div class="tri-5"></div>
                                        </div>
                                        <div class="col-lg-3"><div class="tri-6"></div></div>
                                        <div class="col-lg-3"><div class="top-border"><div class="circle"></div></div></div>
                                    </div>

                                    <div class="col-lg-12 tri-content triangle">
                                        <div class="row">
                                            <div class="col-lg-3"><div class="tri-8"></div></div>
                                            <div class="col-lg-3"><div class="tri-9"></div></div>
                                            <div class="col-lg-3"><div class="tri-10"></div></div>
                                            <div class="col-lg-3"><div class="tri-11"></div></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 tri-content">
                                        <div class="row">
                                            <div class="col-lg-3"><div class="tri-12">
                                                    <h1>5</h1>
                                                    <span class="child-1"> Global </span>
                                                    <span class="child-2"> certificates </span>
                                                </div></div>
                                            <div class="col-lg-3"><div class="tri-13"></div></div>
                                            <div class="col-lg-3 col-12"><div class="tri-14">
                                                    <h1>30</h1>
                                                    <span> Years of </span>
                                                    <span> experience </span>
                                                </div></div>
                                            <div class="col-lg-3"><div class="tri-15"></div></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 tri-content">
                                        <div class="row">
                                            <div class="col-lg-3"><div class="tri-16"></div></div>
                                            <div class="col-lg-3"><div class="tri-17"></div></div>
                                            <div class="col-lg-3"><div class="tri-18"></div></div>
                                            <div class="col-lg-3"><div class="tri-19"></div></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 tri-content">
                                        <div class="row">
                                            <div class="col-lg-3"><div class="border-tri"> <div class="circle"></div></div></div>
                                            <div class="col-lg-3"><div class="tri-20"></div></div>
                                            <div class="col-lg-3"><div class="tri-21">
                                                    <h1>1000</h1>
                                                    <span> Produced </span>
                                                    <span> devices </span>
                                                </div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-8 -->
                </div>


            </div>
        </div>
        <!-- /.container -->
    </section>

@endsection


@section('head')


    <link rel="stylesheet" href="{{asset('template/plugins/swiper/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/anime/animate.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/about-us.css')}}">

@endsection

@section('script')

    <script src="{{asset('template/plugins/swiper/swiper.min.js')}}"></script>
    <script src="{{asset('template/plugins/lazy/lazy.min.js')}}"></script>
    <script src="{{asset('template/js/custom.js')}}"></script>


@endsection

@section('seo')


    <meta name="author" content="amin nourbaghaei">

    <meta name="robots" content="noindex,nofollow" />

@endsection


