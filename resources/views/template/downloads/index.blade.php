@extends('template.app')
@section('content')

    <section  class="bread-container">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10">
                <ul>
                    <li><a href="#"> Download  </a></li>
                </ul>
            </div>
        </div>
    </section>


    @foreach($downloads as $cat_download)
       <section class="box_style f-down-content">
           <div class="download-content box_style">
               <div class="container ">
                   <div class="col-lg-8 down-col">
                       <h3>{{get_category($cat_download[0]->category)}}</h3>
                       <ul>
                           @foreach($cat_download as $download)
                           <li>
                               <div class="row">
                                   <div class="col-lg-6 col-md-6 title-BX"> <span>{{$download->title}} </span></div>

                                   @if($download->hasMedia(config('cms.collection-download')))
                                       <div class="col-lg-6 col-md-6 btn-BX download-box"><a target="_blank" href="{{$download->getFirstMediaUrl(config('cms.collection-download'))}}" class="btn draw-border" download>Download</a></div>
                                   @else
                                       <div class="col-lg-6 col-md-6 btn-BX download-box"><button class="btn draw-border">Coming Soon</button></div>

                                   @endif
                               </div>
                           </li>
                           @endforeach


                       </ul>
                   </div>
               </div>
           </div>
       </section>
    @endforeach


@endsection

@section('head')

    <link rel="stylesheet" href="{{asset('template/plugins/anime/animate.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/contact.css')}}">

@endsection



@section('script')

    <script src="{{asset('template/js/download.js')}}"></script>

@endsection


@section('seo')


    <meta name="author" content="amin nourbaghaei">

    <meta name="robots" content="noindex,nofollow" />

@endsection



