$(document).ready(function () {
    // menu fixed js code
    $(window).scroll(function () {
        var window_top = $(window).scrollTop() + 1;
        if (window_top > 50) {
            $('.navigation').addClass('menu_fixed animated fadeInDown');
        } else {
            $('.navigation').removeClass('menu_fixed animated fadeInDown');
        }
    });

    $(".product-carousel").owlCarousel({
        rtl: true,
        loop: true,
        margin: 10,
        items: 1,
        nav: true,
        navText: ['<i class="fa fa-angle-double-right"></i>', '<i class="fa fa-angle-double-left"></i>'],
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                slideBy: 1
            },
            576: {
                items: 1,
                slideBy: 1
            },
            768: {
                items: 2,
                slideBy: 2
            },
            992: {
                items: 3,
                slideBy: 2
            },
            1400: {
                items: 3,
                slideBy: 1
            }
        }
    });
})