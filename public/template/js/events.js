$(document).ready(function() {
    // executes when HTML-Document is loaded and DOM is ready
    // menu fixed js code
    $(window).scroll(function () {
        var window_top = $(window).scrollTop() + 1;
        if (window_top > 50) {
            $('.navigation').addClass('menu_fixed animated fadeInDown');
        } else {
            $('.navigation').removeClass('menu_fixed animated fadeInDown');
        }
    });
// breakpoint and up
    $(window).resize(function(){
        if ($(window).width() >= 980){

            // when you hover a toggle show its dropdown menu
            $(".navbar .dropdown-toggle").hover(function () {
                $(this).parent().toggleClass("show");
                $(this).parent().find(".dropdown-menu").toggleClass("show");
            });

            // hide the menu when the mouse leaves the dropdown
            $( ".navbar .dropdown-menu" ).mouseleave(function() {
                $(this).removeClass("show");
            });

            // do something here
        }

    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 5,
        loop: true,
        freeMode: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
        direction: 'vertical',
        spaceBetween: 10,
        loop:true,
        loopedSlides: 5, //looped slides should be the same
        thumbs: {
            swiper: galleryThumbs,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
    });

    var product_galleryThumbs = new Swiper('.product_gallery-thumbs', {
        spaceBetween: 20,
        slidesPerView: 3,
        loop: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var product_galleryTop = new Swiper('.product_gallery-top', {
        spaceBetween: 10,
        loop:true,
        slidesPerView: 3,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: product_galleryThumbs,
        },
    });

    var news_left_slider = new Swiper('.news-left-slider' , {
        direction: 'vertical',
        loop:true,
        loopedSlides: 3,
        slidesPerView: 3,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        breakpoints: {
            576: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 20
            },
        }
    });
    var news_right_slider = new Swiper('.news-right-slider' , {
        direction: 'vertical',
        loop:true,
        loopedSlides: 3,
        slidesPerView: 3,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
    });
// company
    var company_galleryThumbs = new Swiper('.company-gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 3,
        loop: true,
        freeMode: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var company_galleryTop = new Swiper('.company-gallery-top', {
        spaceBetween: 10,
        loop:true,
        loopedSlides: 5, //looped slides should be the same
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: company_galleryThumbs
        },
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
    });

// document ready
});
