(function($) {


    $('#form-contact').validate({
        rules : {
            email : {
                required: true,
                email: true
            },
            phone : {
                required: true,
               minlength:11, maxlength:11,digits: true
            }
        },
        message : {
            minlength: "at least 11 numbers"
        },
        onfocusout: function(element) {
            $(element).valid();
        },
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "",
        remote: "",
        email: "",
        url: "",
        date: "",
        dateISO: "",
        number: "",
        digits: "",
        creditcard: "",
        equalTo: ""
    });
})(jQuery);