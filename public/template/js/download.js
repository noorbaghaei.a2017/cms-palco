$(document).ready(function () {
    // menu fixed js code
    $(window).scroll(function () {
        var window_top = $(window).scrollTop() + 1;
        if (window_top > 50) {
            $('.navigation').addClass('menu_fixed animated fadeInDown');
        } else {
            $('.navigation').removeClass('menu_fixed animated fadeInDown');
        }
    });

    /*------------------
      Background Set
  --------------------*/
    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    $(function() {
        $('.lazy').lazy({
            effect: "fadeIn",
            effectTime: 1000,
            threshold: 0
        });
    });
});