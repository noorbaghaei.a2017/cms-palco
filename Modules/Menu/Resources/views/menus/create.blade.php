@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}}</h2>
                        <small>
                           {{__('menu::menus.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">

@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('menus.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title" required>
                                </div>


                                <div class="col-sm-3">
                                    <label for="manual" class="form-control-label">{{__('cms.manual-address')}} </label>
                                    <input type="text" name="manual" class="form-control" id="manual">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="href"  class="form-control-label">{{__('cms.address')}}  </label>
                                    <select dir="rtl" class="form-control" id="href" name="href" >


                                        <optgroup label=" {{__('menu::menus.collect')}}">
                                            @foreach($menus as $menu)
                                                <option value="{{$menu->href}}">{{$menu->title}}</option>
                                            @endforeach

                                        </optgroup>
                                        <optgroup label="{{__('page::pages.collect')}}">
                                            @foreach($pages as $page)
                                                <option value="{{$page->href}}">{{$page->title}}</option>
                                            @endforeach

                                        </optgroup>
                                        <optgroup label=" {{__('product::products.collect')}}">
                                            @foreach($products as $product)
                                                <option value="{{route('products.single',['product'=>$product->slug])}}">{{$product->title}}</option>
                                            @endforeach

                                        </optgroup>
                                        <optgroup label=" لیست دسته بندی محصولات">
                                            @foreach($categories_product as $category)
                                                <option value="{{route('categories.product',['category'=>$category->slug])}}">{{$category->symbol}}</option>
                                            @endforeach

                                        </optgroup>
                                        <optgroup label=" {{__('portfolio::portfolios.collect')}}">
                                            @foreach($portfolios as $portfolio)
                                                <option value="{{route('portfolios.single',['portfolio'=>$portfolio->slug])}}">{{$portfolio->title}}</option>
                                            @endforeach

                                        </optgroup>
                                        <optgroup label=" {{__('article::articles.collect')}}">
                                            @foreach($articles as $article)
                                                <option value="{{route('articles.single',['article'=>$article->slug])}}">{{$article->title}}</option>
                                            @endforeach

                                        </optgroup>
                                        <optgroup label=" {{__('information::informations.collect')}}">
                                            @foreach($informations as $information)
                                                <option value="{{route('informations.single',['information'=>$information->slug])}}">{{$information->title}}</option>
                                            @endforeach

                                        </optgroup>


                                    </select>

                                </div>

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" name="order" value="{{old('order')}}" class="form-control" id="order">
                                </div>


                            </div>
                            <div class="form-group row">

                                <div class="col-sm-3">
                                    <label for="icon" class="form-control-label">{{__('cms.icon')}} </label>
                                    <input type="text" name="icon" value="{{old('icon')}}" class="form-control" id="icon">
                                </div>

                              <div class="col-sm-3">
                                  <span class="text-danger">*</span>
                                  <label for="parent" class="form-control-label">{{__('cms.parent')}}  </label>
                                  <select dir="rtl" class="form-control" id="parent" name="parent" required>

                                          <option selected value="-1">خودم</option>
                                          @foreach($parent_menus as $menu)
                                              <option value="{{$menu->token}}">{{$menu->title}}</option>
                                          @endforeach

                                  </select>
                              </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="list" class="form-control-label">{{__('cms.sub-menu')}} </label>
                                    <select class="form-control" id="list" name="list" required>
                                        @foreach($listmenus as $list)
                                            <option value="{{$list->token}}">{{$list->label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="columns" class="form-control-label">{{__('cms.columns')}} </label>
                                    <input type="number" name="columns" value="{{old('column')}}" class="form-control" id="columns">
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="column" class="form-control-label">{{__('cms.column')}} </label>
                                    <input type="number" name="column" value="{{old('column')}}" class="form-control" id="column">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="symbol" class="form-control-label">{{__('cms.symbol')}} </label>
                                    <input type="text" name="symbol" value="{{old('symbol')}}" class="form-control" id="symbol" required>
                                </div>
                            </div>

                            @include('core::layout.create-button')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    parent: {
                        required: true
                    },
                    list: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },
                    symbol: {
                        required: true
                    },

                    columns: {
                        number: true,
                        max:4,
                        min:1
                    },
                    column: {
                        number: true,
                        max:4,
                        min:1
                    },


                },
                messages: {
                    title:"عنوان الزامی است",
                    parent: "والد  لزامی است",
                    list: "زیر منو  الزامی است",
                    symbol: "نماد  الزامی است",
                    order: " فرمت  نادرست",
                    column: " اندازه  نادرست",
                    columns: " اندازه  نادرست",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection

