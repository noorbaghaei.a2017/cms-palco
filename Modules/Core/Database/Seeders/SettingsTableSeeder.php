<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('settings')->delete();

        $settings=[

            [
                'name' => 'palcotek',
                'address' => 'Koelner Strasse 60 , 50859 , Cologne , Germany',
                'domain' => 'http://palcotek.com',
                'country' => 'ir',
                'email' => 'info@palcotek.com',
                'phone' => json_encode(['02134567','0214568329']),
                'pre_phone' => '+98',
                'mobile' => json_encode(['09195994032','09123456751','09195432910'])
            ]
        ];

        DB::table('settings')->insert($settings);

        $setting=Setting::latest()->firstOrFail();

        $setting->seo()->create([
            'title'=>'',
            'description'=>'',
            'keyword'=>'',
            'robots'=>json_encode(['index','follow']),
            'canonical'=>env('APP_URL'),
        ]);
    }
}
