<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Analyzer extends Model
{
    protected $fillable = ['view','like','star'];

    public function analyzerable()
    {
        return $this->morphTo();
    }
}
