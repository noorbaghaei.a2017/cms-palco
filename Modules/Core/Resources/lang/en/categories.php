<?php
return [
    "text-create"=>"you can create your category",
    "text-edit"=>"you can edit your category",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"products list",
    "singular"=>"category",
    "collect"=>"categories",
    "permission"=>[
        "product-full-access"=>"categories full access",
        "product-list"=>"categories list",
        "product-delete"=>"category delete",
        "product-create"=>"category create",
        "product-edit"=>"edit category",
    ]
];
