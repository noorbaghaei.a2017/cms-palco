@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')
    <div class="padding">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                            با استفاده از فرم زیر میتوانید مقاله جدید اضافه کنید.
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        @include('core::layout.alert-danger')
                        <form role="form" method="post" action="{{route('articles.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">عنوان </label>
                                    <input type="text" name="title" class="form-control" id="title">
                                </div>
                                <div class="col-sm-6">
                                    <label for="title" class="form-control-label">تصویر شاخص </label>
                                    @include('core::layout.load-single-image')
                                </div>


                            </div>
                            <div class="form-group row">

                                <div class="box m-b-md">
                                    <div class="box m-b-md">

                                        <textarea name="text" class="form-control my-editor"></textarea>


                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="excerpt" class="form-control-label">خلاصه </label>
                                    <input type="text" name="excerpt" class="form-control" id="excerpt" autocomplete="off">
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="tags"  class="form-control-label"> برچسب ها  </label>
                                    <select dir="rtl" class="form-control" multiple="multiple" id="tags" name="tags[]">

                                    </select>

                                </div>
                            </div>


                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.create')}} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


