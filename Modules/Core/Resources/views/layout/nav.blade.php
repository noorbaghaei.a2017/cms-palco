

    <li>
        <a href="{{route('contacts.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="fa fa-envelope"></i>
                              </span>
            <span class="nav-label">
                @if($contact_count!=0)
				<b class="label info rounded">{{$contact_count}}</b>
                @endif
			</span>
            <span class="nav-text">{{__('cms.messages')}}</span>
        </a>
    </li>






    <li>
        <a href="{{route('dashboard.setting.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="ion-android-settings"></i>
                              </span>
            <span class="nav-text">{{__('cms.setting')}}</span>
        </a>
    </li>

