<?php


namespace Modules\Core\Http\Controllers;


use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;

trait HasGallery
{



    public  function gallery(Request $request,$token){
        try {
            $item=$this->entity->whereToken($token)->first();
            $medias= $item->getMedia(config('cms.collection-images'));
            return view($this->route_gallery_index,compact('item','medias'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    public  function galleryStore(Request $request,$token){
        try {

            $item=$this->entity->whereToken($token)->first();
            if($request->has('gallery')){
                $item->addMedia($request->file('gallery'))->toMediaCollection(config('cms.collection-images'));
                return redirect(route($this->route_gallery))->with('message',__($this->notification_store));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }

    public  function galleryDestroy(Request $request,$media){
        try {
            Media::find($media)->delete();

            return redirect(route($this->route_gallery))->with('message',__($this->notification_delete));

        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }
}
