<?php


namespace Modules\Core\Helper\Trades;


trait TimeAttribute
{

    public  function getTimeCreateAttribute(){

        return $this->created_at->format('d M');
    }
    public  function getAgoTimeUpdateAttribute(){

        return $this->updated_at->ago();
    }
    public  function getAgoTimeAttribute(){

        return $this->created_at->ago();
    }


    public  function getMonthYearAttribute(){

        return $this->created_at->format('d M');
    }
}
