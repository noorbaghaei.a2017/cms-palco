<?php


namespace Modules\Core\Helper;


use Illuminate\Database\Eloquent\Model;
use Modules\Carousel\Entities\Carousel;
use Modules\Core\Entities\Category;
use Modules\Customer\Entities\Customer;
use Modules\Event\Entities\Event;
use Modules\Product\Entities\Product;
use Modules\Store\Entities\Store;

class CoreHelper
{
    public static function checkSubCategory(Model $category,$token){

        if($category->parent==0){
            return "";
        }
        else{
            if(Category::whereId($category->parent)->first()->token==$token){
                return "selected";
            }
            return "";
        }

    }

    public static function hasSlider(){

        if(Carousel::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
    public static function hasProduct(){

        if(Product::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasEvent(){

        if(Event::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasStore(){

        if(Store::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasBrand(){

        if(Store::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }

    public static function hasCustomer(){

        if(Customer::latest()->count() > 0) {
            return true;
        }
        else{
            return  false;
        }

    }
}
