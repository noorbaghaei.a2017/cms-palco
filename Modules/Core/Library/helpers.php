<?php

use Modules\Core\Entities\Category;
use Modules\Core\Entities\User;
use Illuminate\Support\Arr;
use Modules\Core\Entities\Setting;
use Modules\Menu\Entities\Menu;
use Nwidart\Modules\Facades\Module;

if (!function_exists("Address_Project")) {

    function Address_Project(string $url)
    {

        return Module::asset($url);

    }
}


if(! function_exists('Address_Module')){

    function Address_Module(string $name){

        return Module::assetPath($name) ;
    };

}

if(! function_exists('hasModule')){

    function hasModule(string $module){

        return array_key_exists($module,Module::getByStatus(1)) ?true :false ;
    };

}


if(! function_exists('destroyMedia')){

    function destroyMedia($item,$type){


        if($item->Hasmedia($type)){
            $item->clearMediaCollection($type);
        }
    };

}

if(! function_exists('findAvatar')){

    function findAvatar($email,$size=200){


        $default = "https://www.somewhere.com/homestar.jpg";
        $size = 200;
         return $url = "https://www.gravatar.com/avatar/"
           . md5( strtolower( trim( $email ) ) ) .
           "?d=" . urlencode( $default ) .
           "&s=" . $size;


    };

}
if(! function_exists('statusUrl')){

    function statusUrl($url,$method='GET',$param=null){

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, true);
        curl_setopt($curlHandle, CURLOPT_NOBODY  , true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_exec($curlHandle);
        return curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);


    };

}

if(! function_exists('fullName')){

    function fullName($fname,$lname){

        return (trim($fname). " ".trim($lname));

    };

}

if(! function_exists('checkZeroFirst')){

    function checkZeroFirst($string){
        if($string[0]==='0')
        {
            return substr($string,1);
        }
        return $string;
    }

}


if(! function_exists('setting')){

    function setting($string){
        if(is_string($string))
        {
            return Setting::latest()->first()->$string;
        }
        return "";
    }

}

if(! function_exists('checkboxStatus')){

    function checkboxStatus($status){
        return ($status=="on") ? 1 : 0;
    }

}


if(! function_exists('findImageAuthor')){

    function findImageAuthor($user,$type,$avatar=null){

        try {
            $model=User::whereId($user)->firstOrFail();
            if(!$model->Hasmedia($type))
            {
                if(isset($avatar) && $avatar && statusUrl(findAvatar($model->email,200))===200){
                    return findAvatar($model->email,200);
                }

                else{

                    return asset('img/no-img.gif');
                }
            }
            else{
                return $model->getFirstMediaUrl($type);
            }
        }catch (Exception $exception){
            return "";
        }


    }

}
if(! function_exists('findNameAuthor')){

    function findNameAuthor($user){

        try {
            $model=User::whereId($user)->firstOrFail();
            return fullName($model->first_name,$model->last_name);
        }catch (Exception $exception){
            return "";
        }


    }

}

if(! function_exists('calcPrePhone')){

    function calcPrePhone($country){

        try {
           switch ($country){
               case 'ir':
                   return "+98";
                   break;
               case 'ar':
                   return "+34";
                   break;
               case 'de':
                   return "+49";
                   break;
               default :
                   return " ";
                   break;
           }
        }catch (Exception $exception){
            return "";
        }


    }

}




if(! function_exists('numPages')){

    function numPages($file) {
        $pdftext = file_get_contents($file);
        $count = preg_match_all("/\/Page\W/", $pdftext, $dummy);
        if(!is_null( $count)){
            return $count;
        }
        else{
            return null;
        }
    }

}

if(! function_exists('array_get')){



    function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }

}

if(! function_exists('get_category')){



    function get_category($id)
    {
        return Category::find($id)->symbol;
    }

}

if(! function_exists('child')){



    function child($id)
    {
        return Menu::where('parent',$id)->get();
    }

}

if(! function_exists('childCount')){



    function childCount($id)
    {
        return Menu::where('parent',$id)->count();
    }

}

if(! function_exists('childColumn')){



    function childColumn($id,$column)
    {
        return Menu::where('parent',$id)
            ->where('column',$column)->where('href','<>','@')->orderBy('order','desc')->get();
    }

}

if(! function_exists('hasTitleMegaMenu')){



    function hasTitleMegaMenu($id,$column)
    {
        return Menu::where('parent',$id)->where('column',$column)->where('href','@')->orderBy('order','desc')->first();
    }

}

if(! function_exists('showIp')){



    function showIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

}

if(! function_exists('loadCountry')){



    function loadCountry()
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET','https://restcountries.eu/rest/v2/all');
        if($res->getStatusCode()==200){
            $res = $res->getBody();
            $res = json_decode($res);
            $result= response()->json($res);
            $array=(array)$result;
            return $array['original'];
        }else{
            return [];
        }

    }

}


































