@extends('client::layouts.master')

@section('content')
    <h1>Hello World</h1>

    <p>
        This view is loaded from module: {!! config('client.name') !!}
    </p>
    <a class="dropdown-item"
       onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">{{__('cms.logout')}}</a>
    <form id="frm-logout" action="{{route('logout')}}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
@endsection
