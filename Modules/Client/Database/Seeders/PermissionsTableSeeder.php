<?php

namespace Modules\Client\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\Client;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Client::class)->delete();

        Permission::create(['name'=>'client-list','model'=>Client::class,'created_at'=>now()]);
        Permission::create(['name'=>'client-create','model'=>Client::class,'created_at'=>now()]);
        Permission::create(['name'=>'client-edit','model'=>Client::class,'created_at'=>now()]);
        Permission::create(['name'=>'client-delete','model'=>Client::class,'created_at'=>now()]);
    }
}
