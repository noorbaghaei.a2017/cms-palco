<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
Route::group(["prefix"=>"user"],function() {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('client.login');
    Route::post('/login', 'Auth\LoginController@login')->name('client.login.submit');
    Route::post('/logout', 'Auth\LoginController@l')->name('client.logout');
});
Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/clients', 'ClientController');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/clients', 'ClientController@search')->name('search.client');
    });

});


