
@include('core::layout.modules.index',[

    'title'=>__('page::pages.index'),
    'items'=>$items,
    'parent'=>'page',
    'model'=>'page',
    'directory'=>'pages',
    'collect'=>__('page::pages.collect'),
    'singular'=>__('page::pages.singular'),
    'create_route'=>['name'=>'pages.create'],
    'edit_route'=>['name'=>'pages.edit','name_param'=>'page'],
    'destroy_route'=>['name'=>'pages.destroy','name_param'=>'page'],
     'search_route'=>true,
        'datatable'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
          __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
        __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],

])
