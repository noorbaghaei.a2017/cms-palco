@extends('core::layout.panel')
@section('pageTitle',__('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                            {{__('download::downloads.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        @include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('downloads.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title"  value="{{old('title')}}" class="form-control" id="title" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>
                                <div class="col-sm-3">
                                    <label for="file" class="form-control-label">{{__('cms.file')}} </label>
                                    <input type="file" name="download"   class="form-control" id="file" >
                                </div>


                            </div>
                            <div class="form-group row">
                            <div class="col-sm-3">
                                <span class="text-danger">*</span>
                                <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                <input type="number" name="order" value="{{old('order')}}" class="form-control" id="order">
                            </div>
                            </div>
                            <div class="form-group row">
                                <span class="text-danger">*</span>
                                <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                <div class="box m-b-md">
                                    <div class="box m-b-md">
                                        @include('core::layout.text-editor',['item'=>null])
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" name="excerpt" value="{{old('excerpt')}}" class="form-control" id="excerpt" autocomplete="off" required>
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="category" class="form-control-label">{{__('cms.category')}}  </label>
                                    <select dir="rtl" class="form-control" id="category" name="category" required>
                                        @foreach($categories as $category)
                                            <option value="{{$category->token}}">{{$category->title}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.create')}} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },
                    text: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    category: {
                        required: true
                    },


                },
                messages: {
                    title:"عنوان الزامی است",
                    text: "متن  لزامی است",
                    order: " فرمت  نادرست",
                    excerpt: "خلاصه  الزامی است",
                    category: "دسته بندی  الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
