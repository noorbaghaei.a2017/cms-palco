<?php

namespace Modules\Article\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;


class Article extends Model implements HasMedia , Feedable
{
    use HasTags,Sluggable,HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','text','excerpt','token','slug','user'];


    public function getRouteKeyName()
    {
       return multiRouteKey();
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }




    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }

    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->performOnCollections(config('cms.collection-image'));
    }


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function toFeedItem()
    {
        return FeedItem::create()
            ->id($this->id)
            ->title($this->title)
            ->summary($this->excerpt)
            ->link('articles/'.$this->slug)
            ->author('amin nourbaghaei')
            ->updated($this->updated_at);
    }

    public static function getFeedItems()
    {
        return Article::all();
    }
}
