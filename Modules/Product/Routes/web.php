<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/products', 'ProductController')->only('create','store','destroy','update','index','edit');
    Route::group(["prefix"=>'products'], function () {
        Route::get('/gallery/{product}', 'ProductController@gallery')->name('product.gallery');
        Route::post('/gallery/store/{product}', 'ProductController@galleryStore')->name('product.gallery.store');
        Route::get('/gallery/destroy/{media}', 'ProductController@galleryDestroy')->name('product.gallery.destroy');
    });
    Route::group(["prefix"=>'search'], function () {
        Route::post('/products', 'ProductController@search')->name('search.product');
    });

    Route::group(["prefix"=>'product/questions'], function () {
        Route::get('/{product}', 'ProductController@question')->name('product.questions');
        Route::get('/create/{product}', 'ProductController@questionCreate')->name('product.question.create');
        Route::post('/store/{product}', 'ProductController@questionStore')->name('product.question.store');
        Route::delete('/destroy/{question}', 'ProductController@questionDestroy')->name('product.question.destroy');
        Route::get('/edit/{product}/{question}', 'ProductController@questionEdit')->name('product.question.edit');
        Route::patch('/update/{question}', 'ProductController@questionUpdate')->name('product.question.update');
    });

    Route::group(["prefix"=>'product/categories'], function () {
        Route::get('/', 'ProductController@categories')->name('product.categories');
        Route::get('/create', 'ProductController@categoryCreate')->name('product.category.create');
        Route::post('/store', 'ProductController@categoryStore')->name('product.category.store');
        Route::get('/edit/{category}', 'ProductController@categoryEdit')->name('product.category.edit');
        Route::patch('/update/{category}', 'ProductController@categoryUpdate')->name('product.category.update');

    });

});
