<?php


namespace Modules\Product\Helper;


use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class ProductHelper
{

    public static function checkSubProduct(Model $product,$token){

        if($product->parent==0){
            return "";
        }
        else{
            if(Product::whereId($product->parent)->first()->token==$token){
                return "selected";
            }
            return "";
        }

    }

    public static function findAccessories($product){

        return Product::whereParent($product->id)
            ->whereLevel('0')
            ->get();

    }

    public static function hasAccessories($product){

        if(Product::whereParent($product->id)
            ->whereLevel('0')
            ->count() > 0) {

            return true;
        }
        else{
            return  false;
        }

    }

}
