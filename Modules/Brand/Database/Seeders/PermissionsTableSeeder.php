<?php

namespace Modules\Brand\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Brand\Entities\Brand;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Brand::class)->delete();

        Permission::create(['name'=>'brand-list','model'=>Brand::class,'created_at'=>now()]);
        Permission::create(['name'=>'brand-create','model'=>Brand::class,'created_at'=>now()]);
        Permission::create(['name'=>'brand-edit','model'=>Brand::class,'created_at'=>now()]);
        Permission::create(['name'=>'brand-delete','model'=>Brand::class,'created_at'=>now()]);
    }
}
