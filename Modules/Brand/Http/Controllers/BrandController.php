<?php

namespace Modules\Brand\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Brand\Entities\Brand;
use Modules\Brand\Http\Requests\BrandRequest;

class BrandController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new Brand();
        $this->middleware('permission:brand-list');
        $this->middleware('permission:brand-create')->only(['create','store']);
        $this->middleware('permission:brand-edit' )->only(['edit','update']);
        $this->middleware('permission:brand-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('brand::brands.index',compact('items'));
        }catch (\Exception $exception){
            return dd($exception->getMessage());
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('brand::brands.create');
        }catch (\Exception $exception){
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('brand::brands.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('customer::customers.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(BrandRequest $request)
    {
        try {
            $href=is_null($request->href) ? "#" : $request->href;
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->href=$href;
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }


            if(!$saved){
                return redirect()->back()->with('error',__('brand::brands.error'));
            }else{
                return redirect(route("brands.index"))->with('message',__('brand::brands.store'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('brand::brands.edit',compact('item'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(BrandRequest $request, Brand $brand)
    {
        try {
            $this->entity=$brand;
            $href=is_null($request->href) ? "#" : $request->href;
            $update=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "href"=>$href,
                "order"=>orderInfo($request->input('order')),
            ]);

            $this->entity->replicate();
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$update){
                return redirect()->back()->withErrors('error',__('brand::brands.error'));
            }else{
                return redirect(route("brands.index"))->with('message',__('brand::brands.update'));
            }


        }catch (Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('brand::brands.error'));
            }else{
                return redirect(route("brands.index"))->with('message',__('brand::brands.delete'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }
}
