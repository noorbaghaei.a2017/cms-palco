<?php

namespace Modules\Plan\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Price;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Spatie\Permission\Models\Role;

class Plan extends Model
{
    use Sluggable,TimeAttribute;

    protected $fillable = ['title','text','excerpt','currency','price','period','token','attributes','order','slug'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function price()
    {
        return $this->morphOne(Price::class, 'priceable');
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }

    public  function getPriceFormatAttribute(){

        return number_format($this->price->amount);
    }

     /**
      * @inheritDoc
      */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
