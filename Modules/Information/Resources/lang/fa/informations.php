<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید خبر جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید خبر خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست اخبار",
    "singular"=>"خبر",
    "collect"=>"اخبار",
    "permission"=>[
        "information-full-access"=>"دسترسی کامل اخبار",
        "information-list"=>"لیست اخبار",
        "information-delete"=>"حذف خبر",
        "information-create"=>"ایجاد خبر",
        "information-edit"=>"ویرایش خبر",
    ]
];
