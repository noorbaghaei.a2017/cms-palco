@include('core::layout.modules.index',[

    'title'=>__('core::categories.index'),
    'items'=>$items,
    'parent'=>'information',
    'model'=>'information',
    'directory'=>'informations',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'create_route'=>['name'=>'information.category.create'],
    'edit_route'=>['name'=>'information.category.edit','name_param'=>'category'],
    'pagination'=>false,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])



