@include('core::layout.modules.index',[

    'title'=>__('questions::questions.index'),
    'items'=>$items,
    'parent'=>'store',
    'model'=>'store',
    'directory'=>'stores',
    'collect'=>__('question::questions.collect'),
    'singular'=>__('question::questions.singular'),
    'create_route'=>['name'=>'store.question.create','param'=>$token],
    'edit_route'=>['name'=>'store.question.edit','name_param'=>'store','param'=>'question'],
    'destroy_route'=>['name'=>'store.question.destroy','name_param'=>'question'],
    'pagination'=>false,
    'datatable'=>[
    __('cms.title')=>'title',
     __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
