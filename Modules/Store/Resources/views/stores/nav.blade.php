

    <li>
        <a href="{{route('stores.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('store.icons.store')}}"></i>
                              </span>
            <span class="nav-text"> {{__('store::stores.collect')}}</span>
        </a>
    </li>

