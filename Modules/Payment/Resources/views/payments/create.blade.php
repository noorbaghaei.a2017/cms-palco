@extends('core::layout.panel')
@section('pageTitle', 'ایجاد ')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                            با استفاده از فرم زیر میتوانید درگاه پرداخت جدید اضافه کنید.
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('payments.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title" class="form-control" id="title" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="terminal" class="form-control-label">{{__('cms.terminal')}} </label>
                                    <input type="text" name="terminal" class="form-control" id="terminal" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="username" class="form-control-label">{{__('cms.username')}} </label>
                                    <input type="text" name="username" class="form-control" id="username" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="password" class="form-control-label">{{__('cms.password')}} </label>
                                    <input type="text" name="password" class="form-control" id="password" required>
                                </div>

                            </div>


                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary pull-right">{{__('cms.add')}} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    terminal: {
                        required: true
                    },
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    },


                },
                messages: {
                    title:"عنوان الزامی است",
                    terminal:"ترمینال الزامی است",
                    username: "نام کاربری  لزامی است",
                    password: "کلمه عبور  لزامی است",

                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
