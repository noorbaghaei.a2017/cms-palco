@include('core::layout.modules.index',[

    'title'=>__('payment::pays.index'),
    'items'=>$items,
    'parent'=>'payment',
    'model'=>'pay',
    'directory'=>'pays',
    'collect'=>__('payment::pays.collect'),
    'singular'=>__('payment::pays.singular'),
    'create_route'=>['name'=>'pays.create'],
    'edit_route'=>['name'=>'pays.edit','name_param'=>'pay'],
    'destroy_route'=>['name'=>'pays.destroy','name_param'=>'pay'],
     'search_route'=>true,
    'datatable'=>[
        __('cms.terminal')=>'terminal',
    __('cms.username')=>'username',
     __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
       __('cms.terminal')=>'terminal',
    __('cms.username')=>'username',
    __('cms.password')=>'password',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
