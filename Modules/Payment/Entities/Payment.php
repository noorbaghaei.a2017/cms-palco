<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Payment extends Model
{

    use TimeAttribute;

    protected $fillable = ['user','terminal','username','password','status'];


}
