<?php

namespace Modules\Event\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Event\Entities\Event;
use Modules\Event\Http\Requests\EventRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;

class EventController extends Controller
{

    use HasQuestion,HasCategory,HasGallery;

    protected $entity;

    protected $class;

//category

    protected $route_categories_index='event::categories.index';
    protected $route_categories_create='event::categories.create';
    protected $route_categories_edit='event::categories.edit';
    protected $route_categories='event.categories';

//question

    protected $route_questions_index='event::questions.index';
    protected $route_questions_create='event::questions.create';
    protected $route_questions_edit='event::questions.edit';
    protected $route_questions='events.index';


//gallery

    protected $route_gallery_index='event::events.gallery';
    protected $route_gallery='events.index';



//notification

    protected $notification_store='event::events.store';
    protected $notification_update='event::events.update';
    protected $notification_delete='event::events.delete';
    protected $notification_error='event::events.error';


    public function __construct()
    {
        $this->entity=new Event();

        $this->class=Event::class;

        $this->middleware('permission:event-list');
        $this->middleware('permission:event-create')->only(['create','store']);
        $this->middleware('permission:event-edit' )->only(['edit','update']);
        $this->middleware('permission:event-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('event::events.index',compact('items'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories=Category::latest()->where('model',Event::class)->get();
        return view('event::events.create',compact('categories'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('event::events.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('event::events.index',compact('items','request'));
        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(EventRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->country=$request->input('country');
            $this->entity->address=$request->input('address');
            $this->entity->longitude=$request->input('longitude');
            $this->entity->latitude=$request->input('latitude');
            $this->entity->google_map=$request->input('google_map');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->analyzer()->create();

            $this->entity->attachTags($request->input('tags'));
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$saved){
                return redirect()->back()->with('error',__('event::events.error'));
            }else{
                return redirect(route("events.index"))->with('message',__('event::events.store'));
            }
        }catch (Exception $exception){
            return abort('500');
        }
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Event::class)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('event::events.edit',compact('item','categories'));
        }catch (\Exception $exception){
            return abort('500');
        }

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(EventRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "country"=>$request->input('country'),
                "address"=>$request->input('address'),
                "longitude"=>$request->input('longitude'),
                "latitude"=>$request->input('latitude'),
                "google_map"=>$request->input('google_map'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>json_encode($request->input('robots')),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->syncTags($request->input('tags'));

            if(!$updated){
                return redirect()->back()->with('error',__('event::events.error'));
            }else{
                return redirect(route("events.index"))->with('message',__('event::events.update'));
            }


        }catch (\Exception $exception){
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            destroyMedia($this->entity,config('cms.collection-image'));
            $this->entity->seo()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('event::events.error'));
            }else{
                return redirect(route("events.index"))->with('message',__('event::events.delete'));
            }

        }catch (\Exception $exception){
            return abort('500');
        }
    }


}
