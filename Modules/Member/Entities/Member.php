<?php

namespace Modules\Member\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Info;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Tags\HasTags;

class Member extends Model implements HasMedia
{
    use HasTags,HasMediaTrait,Sluggable,TimeAttribute;

    protected $fillable = [
        'first_name',
        'last_name',
        'password',
        'order',
        'role',
        'token',
        'slug',
        'mobile',
        'email',
        'user',
        'role'
    ];

    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }

    public  function role(){
        return $this->hasOne(MemberRole::class,'id','role');
    }


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'last_name'
            ]
        ];
    }

    public  function getRoleNameAttribute(){
        return __('cms.'.$this->role()->first()->title);
    }



}
