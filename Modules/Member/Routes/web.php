<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>"member"],function() {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('member.login');
    Route::post('/login', 'Auth\LoginController@login')->name('member.login.submit');
    Route::post('/logout', 'Auth\LoginController@l')->name('member.logout');
});



Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/members', 'MemberController')->only('create','store','destroy','update','index','edit');
    Route::resource('/sides', 'SideController')->only('create','store','update','index','edit');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/sides', 'SideController@search')->name('search.side');
        Route::post('/members', 'MemberController@search')->name('search.member');
    });

});
