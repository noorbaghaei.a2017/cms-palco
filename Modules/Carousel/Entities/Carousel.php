<?php

namespace Modules\Carousel\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Models\Role;

class Carousel extends Model implements HasMedia
{
    use HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','excerpt','text','token','user','order'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(200)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));
    }



}
