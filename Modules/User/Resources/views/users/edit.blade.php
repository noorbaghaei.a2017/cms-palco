@extends('core::layout.panel')
@section('pageTitle', 'ویرایش ')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images') && statusUrl(findAvatar($item->email,200))===200)
                                    <img style="height: auto" src="{{findAvatar($item->email,200)}}" alt="" >

                                @elseif(statusUrl(findAvatar($item->email,200))!==200)

                                    <img style="width: 300px;height: auto" src="{{asset('img/no-img.gif')}}">
                                @else

                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.full-name')}}  : </h6>
                                <h4 style="padding-top: 35px">    {{fullName($item->first_name,$item->last_name)}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.email')}} : </h6>
                                <p>    {{$item->email}}</p>
                            </div>

                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.mobile')}}  : </h6>
                                <p>    {{$item->mobile}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <small>
                                {{__('user::users.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('users.update', ['user' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="firstname" class="form-control-label">{{__('cms.first-name')}}</label>
                                    <input type="text" value="{{$item->first_name}}" name="firstname" class="form-control" id="firstname" required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="lastname" class="form-control-label">{{__('cms.last-name')}}</label>
                                    <input type="text" value="{{$item->last_name}}" name="lastname" class="form-control" id="lastname" required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="mobile" class="form-control-label">{{__('cms.mobile')}}</label>
                                    <input  type="number" value="{{$item->mobile}}" name="mobile" class="form-control" id="mobile" required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>

                            </div>
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label for="roles"  class="form-control-label">{{__('cms.role')}}  </label>
                                    <select dir="rtl" class="form-control" id="roles" name="roles" >

                                        @foreach($roles as $role)
                                            <option value="{{$role}}"{{$item->hasRole($role) ? "selected": ""}}>{{$role}}</option>
                                        @endforeach


                                    </select>

                                </div>
                                <div class="col-sm-3">
                                    <label for="username" class="form-control-label">{{__('cms.username')}}</label>
                                    <input type="text" value="{{$item->username}}" name="username" class="form-control" id="username" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
                                    <input type="text" value="{{$item->email}}" name="email" class="form-control" id="email" required>

                                </div>
                                <div class="col-sm-3">

                                    <label for="current_password" class="form-control-label">{{__('cms.current-password')}} </label>
                                    <input type="password" name="current_password" class="form-control" id="current_password">

                                </div>
                            </div>

                            <div class="row form-group">
                            <div class="col-sm-3">

                                <label for="new_password" class="form-control-label">{{__('cms.new-password')}} </label>
                                <input type="password" name="new_password" class="form-control" id="new_password">

                            </div>
                            </div>

                            @include('core::layout.modules.info-box',['info'=>$item->info])

                            @include('core::layout.update-button')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            $.validator.addMethod("regex",
                function (value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },"Please check your input."
            );
            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    mobile: {
                        number:true,
                        required: true,
                        regex: "^9"
                    },
                    email: {
                        required: true
                    },

                    role: {
                        required: true
                    },
                    username: {
                        required: true
                    },


                },
                messages: {
                    firstname:"نام الزامی است",
                    lastname: "نام خانوادگی  لزامی است",
                    mobile: "موبایل  الزامی است",
                    email: "ایمیل  الزامی است",
                    role: "نقش  الزامی است",
                    username: "نام کاربری  الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
