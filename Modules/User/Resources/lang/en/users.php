<?php
return [
    "text-create"=>"you can create your manager",
    "text-edit"=>"you can edit your manager",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"Update Password Error",
    "index"=>"users list",
    "singular"=>"admin",
    "collect"=>"admins",
    "permission"=>[
        "user-full-access"=>"admins full access",
        "user-list"=>"admins list",
        "user-delete"=>"admin delete",
        "user-create"=>"admin create",
        "user-edit"=>"admin user",
    ]
];
