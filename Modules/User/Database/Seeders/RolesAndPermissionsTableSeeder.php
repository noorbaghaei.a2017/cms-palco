<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\User;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $userManager=User::whereMobile('9195995044')->first();

        $admin=User::whereMobile('9195995050')->first();

        $userWriter=User::whereMobile('9195995043')->first();

        $manager=Role::create(['name'=>'manager','token'=>tokenGenerate()]);

        $writer=Role::create(['name'=>'writer','token'=>tokenGenerate()]);

        $writer->syncPermissions([
            'article-list',
            'article-create',
            'article-edit',
            'article-delete'
        ]);
        $manager->syncPermissions([
            'article-list',
            'article-create',
            'article-edit',
            'article-delete',
            'event-list',
            'event-create',
            'event-edit',
            'event-delete',
            'currency-list',
            'currency-create',
            'currency-edit',
            'information-list',
            'information-create',
            'information-edit',
            'information-delete',
            'menu-list',
            'menu-create',
            'menu-edit',
            'menu-delete',
            'download-list',
            'download-create',
            'download-edit',
            'download-delete',
            'customer-list',
            'customer-create',
            'customer-edit',
            'customer-delete',
            'client-list',
            'client-create',
            'client-edit',
            'client-delete',
            'contact-list',
            'contact-edit',
            'contact-delete',
            'member-list',
            'member-create',
            'member-edit',
            'side-list',
            'side-create',
            'side-edit',
            'carousel-list',
            'carousel-create',
            'carousel-edit',
            'carousel-delete',
            'page-list',
            'page-create',
            'page-edit',
            'page-delete',
            'service-list',
            'service-create',
            'service-edit',
            'service-delete',
            'brand-list',
            'brand-create',
            'brand-edit',
            'brand-delete',
            'store-list',
            'store-create',
            'store-edit',
            'store-delete',
            'comment-list',
            'comment-edit',
            'comment-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'advantage-list',
            'advantage-create',
            'advantage-edit',
            'advantage-delete',
            'property-list',
            'property-create',
            'property-edit',
            'property-delete',
            'questions-list',
            'questions-create',
            'questions-edit',
            'questions-delete',
            'product-list',
            'product-create',
            'product-edit',
            'product-delete',
            'portfolio-list',
            'portfolio-create',
            'portfolio-edit',
            'portfolio-delete',
            'plan-list',
            'plan-create',
            'plan-edit',
            'plan-delete',
            'sms-list',
            'sms-create',
            'sms-edit',
            'sms-delete',
            'payment-list',
            'payment-create',
            'payment-edit',
            'payment-delete',
            'pay-list',
            'pay-create',
            'pay-edit',
            'pay-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
        ]);

        //core
        //article
        //event
        //information
        //menu
        //download
        //customer
        //client
        //member
        //carousel
        //page
        //service
        //brand
        //store
        //comment
        //question
        //product
        //portfolio
        //plan
        //sms
        //payment
        //user

        $userManager->assignRole($manager);
        $admin->assignRole($manager);
        $userWriter->assignRole($writer);

    }
}
