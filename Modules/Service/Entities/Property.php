<?php

namespace Modules\Service\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\Permission\Models\Role;

class Property extends Model
{
    use TimeAttribute;

    protected $fillable = ['title','text','title','icon','user','token','icon','order'];

}
