<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید مزیت جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید مزیت خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "error"=>"خطا",
    "index"=>"لیست مزیت ها",
    "singular"=>"مزیت",
    "collect"=>"مزیت ها",
    "permission"=>[
        "advantage-full-access"=>"دسترسی کامل به مزیت ها",
        "advantage-list"=>"لیست مزیت ها",
        "advantage-delete"=>"حذف مزیت",
        "advantage-create"=>"ایجاد مزیت",
        "advantage-edit"=>"ویرایش مزیت",
    ]
];
