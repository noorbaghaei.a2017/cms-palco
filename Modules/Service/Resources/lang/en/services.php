<?php
return [
    "text-create"=>"you can create your service",
    "text-edit"=>"you can edit your service",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"services list",
    "singular"=>"service",
    "collect"=>"services",
    "permission"=>[
        "service-full-access"=>"services full access",
        "service-list"=>"services list",
        "service-delete"=>"service delete",
        "service-create"=>"service create",
        "service-edit"=>"edit service",
    ]
];
